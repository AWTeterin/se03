package ru.teterin.tm.entity;

import ru.teterin.tm.util.DateUtil;

import java.util.Date;
import java.util.UUID;

public class Project {

    private UUID id = UUID.randomUUID();
    private String name = "";
    private String description = "";
    private Date dateStart = new Date();
    private Date dateEnd = new Date();

    public Project() {
    }

    public Project(UUID id, String name, String description, Date dateStart, Date dateEnd) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    @Override
    public String toString() {
        return "Project{" +
                "id='" + id.toString() + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", dateStart=" + DateUtil.DATE_FORMAT.format(dateStart) +
                ", dateEnd=" + DateUtil.DATE_FORMAT.format(dateEnd) +
                '}';
    }

}
