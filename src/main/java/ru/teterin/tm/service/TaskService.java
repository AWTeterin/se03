package ru.teterin.tm.service;

import ru.teterin.tm.entity.Task;
import ru.teterin.tm.exception.IncorrectDataException;
import ru.teterin.tm.exception.IncorrectUUIDException;
import ru.teterin.tm.exception.ObjectExistException;
import ru.teterin.tm.exception.ObjectNotFoundException;
import ru.teterin.tm.repository.TaskRepository;
import ru.teterin.tm.util.ParseUtil;

import java.util.*;

public class TaskService {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository){
        this.taskRepository = taskRepository;
    }

    public Task findOne(String id) throws IncorrectUUIDException, ObjectNotFoundException {
        Task task = taskRepository.findOne(ParseUtil.toUUID(id));
        if(task != null)
            return task;
        throw new ObjectNotFoundException();
    }

    public Collection<Task> findAll(){
        return taskRepository.findAll().values();
    }

    public Collection<Task> findAllByProjectId(String id) throws IncorrectUUIDException{
        UUID uuid = ParseUtil.toUUID(id);
        List<Task> projectTasks = new LinkedList<>();
        for(Task task : findAll()) {
            if(task.getProjectId().toString().equals(id))
                projectTasks.add(task);
        }
        return projectTasks;
    }

    // Добавление новой задачи
    public Task persist(Task task) throws ObjectExistException, ObjectNotFoundException {
        if(task != null)
            return taskRepository.persist(task);
        throw new ObjectNotFoundException();
    }

    //Добавление/изменение задачи
    public Task merge(Task task) throws ObjectNotFoundException{
        if(task != null)
            return  taskRepository.merge(task);
        throw new ObjectNotFoundException();
    }

    public Task remove(String id) throws IncorrectUUIDException, ObjectNotFoundException {
        UUID uuid = ParseUtil.toUUID(id);
        Task task = taskRepository.remove(uuid);
        if(task != null)
            return task;
        throw new ObjectNotFoundException();
    }

    public void removeAll(){
        taskRepository.removeAll();
    }

    public Task createTask(String id, String name, String description, String dStart, String dEnd) throws IncorrectUUIDException, IncorrectDataException {
        UUID uuid = UUID.randomUUID();
        if(id != null && id != "") uuid = ParseUtil.toUUID(id);
        Date dateStart = ParseUtil.toDate(dStart);
        Date dateEnd = ParseUtil.toDate(dEnd);
        return new Task(uuid, name, description, dateStart, dateEnd);
    }

    public void linkTask(String projectId, String id) throws IncorrectUUIDException, ObjectNotFoundException {
        UUID projectUuid = ParseUtil.toUUID(projectId);
        UUID uuid = ParseUtil.toUUID(id);
        Task task = taskRepository.findOne(uuid);
        if(task != null) {
            task.setProjectId(projectUuid);
            taskRepository.merge(task);
            return;
        }
        throw new ObjectNotFoundException();
    }

}
