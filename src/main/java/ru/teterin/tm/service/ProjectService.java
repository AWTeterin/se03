package ru.teterin.tm.service;

import ru.teterin.tm.entity.Project;
import ru.teterin.tm.exception.IncorrectDataException;
import ru.teterin.tm.exception.IncorrectUUIDException;
import ru.teterin.tm.exception.ObjectExistException;
import ru.teterin.tm.exception.ObjectNotFoundException;
import ru.teterin.tm.repository.ProjectRepository;
import ru.teterin.tm.util.ParseUtil;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

public class ProjectService {

    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository){
        this.projectRepository = projectRepository;
    }

    public Project findOne(String id) throws IncorrectUUIDException, ObjectNotFoundException {
        Project project = projectRepository.findOne(ParseUtil.toUUID(id));
        if(project != null)
            return project;
        throw new ObjectNotFoundException();
    }

    public Collection<Project> findAll(){
        return projectRepository.findAll().values();
    }

    // Добавление новой задачи
    public Project persist(Project project) throws ObjectExistException, ObjectNotFoundException {
        if(project != null)
            return projectRepository.persist(project);
        throw new ObjectNotFoundException();
    }

    //Добавление/изменение задачи
    public Project merge(Project project) throws ObjectNotFoundException {
        if(project != null)
            return projectRepository.merge(project);
        throw new ObjectNotFoundException();
    }

    public Project remove(String id) throws IncorrectUUIDException, ObjectNotFoundException {
        UUID uuid = ParseUtil.toUUID(id);
        Project project = projectRepository.remove(uuid);
        if(project != null)
            return project;
        throw new ObjectNotFoundException();
    }

    public void removeAll(){
        projectRepository.removeAll();
    }

    public Project createProject(String id, String name, String description, String dStart, String dEnd) throws IncorrectUUIDException, IncorrectDataException {
        UUID uuid = UUID.randomUUID();
        if(id != null && id != "")
            uuid = ParseUtil.toUUID(id);
        Date dateStart = ParseUtil.toDate(dStart);
        Date dateEnd = ParseUtil.toDate(dEnd);
        return new Project(uuid, name, description, dateStart, dateEnd);
    }

}
