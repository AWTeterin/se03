package ru.teterin.tm.repository;

import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;
import ru.teterin.tm.exception.ObjectExistException;

import java.util.*;

public class ProjectRepository extends Repository<Project> {

    private Map<UUID, Project> projectMap = new TreeMap<>();
    private TaskRepository taskRepository;

    public ProjectRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Map<UUID, Project> findAll() {
        return projectMap;
    }

    @Override
    public Project findOne(UUID id) {
        return projectMap.get(id);
    }

    //Добавление нового проекта в репозиторий, если нет выбрасываем исключение
    @Override
    public Project persist(Project newObject) throws ObjectExistException {
        if(!projectMap.containsKey(newObject.getId()))
            return projectMap.put(newObject.getId(), newObject);
        else
            throw new ObjectExistException();
    }

    //Добавление нового проекта если его нет в репозитории, иначе замена старого
    @Override
    public Project merge(Project object) {
        return projectMap.put(object.getId(), object);
    }

    @Override
    public Project remove(UUID id) {
        Collection<Task> list= taskRepository.findAll().values();
        list.removeIf(x->x.getProjectId().equals(id));
        return projectMap.remove(id);
    }

    @Override
    public void removeAll() {
        for(Map.Entry<UUID, Task> pairTask : taskRepository.findAll().entrySet()){
            UUID taskId = pairTask.getKey();
            Task task = pairTask.getValue();
            if(task.getProjectId() != null)
                taskRepository.remove(taskId);
        }
        projectMap.clear();
    }

}
