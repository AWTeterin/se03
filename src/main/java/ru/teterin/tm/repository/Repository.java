package ru.teterin.tm.repository;

import ru.teterin.tm.exception.ObjectExistException;

import java.util.Map;
import java.util.UUID;

public abstract class Repository<T> {

    public abstract Map<UUID, T> findAll();

    public abstract T findOne(UUID id);

    public abstract T persist(T newObject) throws ObjectExistException;

    public abstract T merge(T object);

    public abstract T remove(UUID id);

    public abstract void removeAll();

}
