package ru.teterin.tm.repository;

import ru.teterin.tm.entity.Task;
import ru.teterin.tm.exception.ObjectExistException;

import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

public class TaskRepository extends Repository<Task>{

    private Map<UUID, Task> taskMap = new TreeMap<>();

    @Override
    public Map<UUID, Task> findAll() {
        return taskMap;
    }

    @Override
    public Task findOne(UUID id) {
        return taskMap.get(id);
    }

    //Добавление новой задачи если ее нет в репозитории, иначе выбрасываем исключение
    @Override
    public Task persist(Task newObject) throws ObjectExistException {
        if(!taskMap.containsKey(newObject.getId()))
            return taskMap.put(newObject.getId(), newObject);
        else
            throw new ObjectExistException();
    }

    //Добавление новой задачи если ее нет в репозитории, иначе замена старого
    @Override
    public Task merge(Task object) {
        return taskMap.put(object.getId(),object);
    }

    @Override
    public Task remove(UUID id) {
        return taskMap.remove(id);
    }

    @Override
    public void removeAll() {
        taskMap.clear();
    }

}
