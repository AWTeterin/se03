package ru.teterin.tm;

import java.io.IOException;

public class Application {

    public static void main(String[] args) throws IOException {
        new Bootstrap().init();
    }

}
