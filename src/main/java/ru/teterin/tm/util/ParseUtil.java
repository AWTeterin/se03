package ru.teterin.tm.util;

import ru.teterin.tm.exception.IncorrectDataException;
import ru.teterin.tm.exception.IncorrectUUIDException;

import java.text.ParseException;
import java.util.Date;
import java.util.UUID;

public class ParseUtil {

    public static UUID toUUID(String id) throws IncorrectUUIDException {
        UUID uuid;
        try{
            uuid = UUID.fromString(id);
        }
        catch (IllegalArgumentException e){
            throw new IncorrectUUIDException();
        }
        return uuid;
    }

    public static Date toDate(String stringDate) throws IncorrectDataException {
        Date date;
        try{
            date = DateUtil.DATE_FORMAT.parse(stringDate);
        }
        catch(ParseException e){
            throw new IncorrectDataException();
        }
        return date;
    }

}
