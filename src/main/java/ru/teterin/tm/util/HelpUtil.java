package ru.teterin.tm.util;

public class HelpUtil {

    private static final String helpMessage = "help: Show all commands.\n"
            + "project_clear: Remove all projects.\n"
            + "project_create: Create new project.\n"
            + "project_list: Show all projects.\n"
            + "project_edit: Edit selected project.\n"
            + "project_remove: Remove selected project.\n"
            + "project_task: Show all task this project.\n"
            + "task_clear: Remove all tasks.\n"
            + "task_create: Create new task.\n"
            + "task_list: Show all tasks.\n"
            + "task_edit: Edit selected task.\n"
            + "task_link:  Link task to a project.\n"
            + "task_remove: Remove selected task.\n";

    public void getHelpMessage(){
        System.out.println(helpMessage);
    }

}
