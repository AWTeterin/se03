package ru.teterin.tm;

import ru.teterin.tm.enumerated.Command;
import ru.teterin.tm.exception.IncorrectDataException;
import ru.teterin.tm.exception.IncorrectUUIDException;
import ru.teterin.tm.exception.ObjectExistException;
import ru.teterin.tm.exception.ObjectNotFoundException;
import ru.teterin.tm.repository.ProjectRepository;
import ru.teterin.tm.repository.TaskRepository;
import ru.teterin.tm.service.ProjectService;
import ru.teterin.tm.service.TaskService;
import ru.teterin.tm.util.HelpUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;


public class Bootstrap {

    private static final String IO_ERROR = "[INPUT/OUTPUT ERROR]";
    private static final String INCORRECT_COMMAND_MESSAGE = "[COMMAND NOT FOUND]\n";
    private static final String OK = "[OK]\n";
    private static final String INCORRECT_ID = "[INCORRECT ID]\n";
    private static final String INCORRECT_DATE_FORMAT = "[INCORRECT DATE FORMAT]\n";
    private static final String OBJECT_NOT_FOUND = "[OBJECT NOT FOUND]\n";
    private static final String OBJECT_REMOVED = "[OBJECT REMOVED]\n";
    private static final String OBJECTS_REMOVED = "[ALL OBJECTS REMOVED]\n";
    private static final String ENTER_ID = "ENTER ID:";
    private static final String ENTER_NAME = "ENTER NAME:";
    private static final String ENTER_DESCRIPTION = "ENTER DESCRIPTION:";
    private static final String ENTER_DATE_START = "ENTER DATE START:";
    private static final String ENTER_DATE_END = "ENTER DATE START:";
    private static final String START_MESSAGE = "*** WELCOME TO TASK MANAGER ***";
    private static final String INCORRECT_COMMAND = "DEFAULT";
    private static final int NO_ERRORS = 0;

    private static TaskRepository taskRepository = new TaskRepository();
    private static ProjectRepository projectRepository = new ProjectRepository(taskRepository);
    private static ProjectService projectService = new ProjectService(projectRepository);
    private static TaskService taskService = new TaskService(taskRepository);
    private static HelpUtil helpUtil = new HelpUtil();
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public void init() throws IOException {
        System.out.println(START_MESSAGE);
        while(true){
            String string = reader.readLine().trim().toUpperCase();
            Command command;
            try{
                command = Command.valueOf(string);
            }
            catch(IllegalArgumentException e){
                command = Command.valueOf(INCORRECT_COMMAND);
            }
            processingCommand(command);
        }
    }

    public void processingCommand(Command command) throws IOException{
        switch(command){
            case DEFAULT:
                System.out.println(INCORRECT_COMMAND_MESSAGE);
                break;
            case HELP:
                helpUtil.getHelpMessage();
                break;
            case PROJECT_CLEAR:
                System.out.println("[PROJECTS CLEAR]");
                projectService.removeAll();
                System.out.println(OBJECTS_REMOVED);
                break;
            case    PROJECT_CREATE:
                createProject(reader);
                break;
            case    PROJECT_LIST:
                System.out.println("[PROJECTS LIST]");
                System.out.println(projectService.findAll());
                System.out.println(OK);
                break;
            case PROJECT_EDIT:
                editProject(reader);
                break;
            case   PROJECT_REMOVE:
                removeProject(reader);
                break;
            case PROJECT_TASKS:
                projectTasks();
                break;
            case TASK_CLEAR:
                System.out.println("[TASK CLEAR]");
                taskService.removeAll();
                System.out.println(OK);
                break;
            case TASK_CREATE:
                createTask(reader);
                break;
            case TASK_LIST:
                System.out.println("[TASK LIST]");
                System.out.println(taskService.findAll());
                System.out.println(OK);
                break;
            case TASK_EDIT:
                editTask(reader);
                break;
            case TASK_REMOVE:
                removeTask(reader);
                break;
            case TASK_LINK:
                linkToProject(reader);
                break;
            case EXIT:
                reader.close();
                System.exit(NO_ERRORS);
                break;
        }
    }

    public void createProject(BufferedReader reader){
        System.out.println("[PROJECT CREATE]");
        List list = readData(reader);
        if(list != null && list.size() > 3) {
            try {
                projectService.persist(projectService.createProject(null, list.get(0).toString(), list.get(1).toString(),
                                                                                    list.get(2).toString(), list.get(3).toString()));
                System.out.println(OK);
            }
            catch (IncorrectUUIDException e) {
                System.out.println(INCORRECT_ID);
            }
            catch(IncorrectDataException e){
                System.out.println(INCORRECT_DATE_FORMAT);
            }
            catch(ObjectExistException e){
                System.out.println(OBJECT_NOT_FOUND);
            }
            catch (ObjectNotFoundException e) {
                System.out.println(OBJECT_NOT_FOUND);
            }
        }
    }

    public void editProject(BufferedReader reader){
        System.out.println("[PROJECT EDIT]");
        System.out.println(ENTER_ID);
        String id = "";
        try{
            id = reader.readLine();
        }
        catch(IOException e){
            System.out.println(IO_ERROR);
        }
        List list = readData(reader);
        if(list != null && list.size() > 3) {
            try {
                projectService.merge(projectService.createProject(id, list.get(0).toString(), list.get(1).toString(),
                                                                                    list.get(2).toString(), list.get(3).toString()));
                System.out.println(OK);
            }
            catch (IncorrectUUIDException e) {
                System.out.println(INCORRECT_ID);
            }
            catch(IncorrectDataException e){
                System.out.println(INCORRECT_DATE_FORMAT);
            }
            catch (ObjectNotFoundException e) {
                System.out.println(OBJECT_NOT_FOUND);
            }
        }
    }

    public void removeProject(BufferedReader reader){
        System.out.println("[PROJECT REMOVE]");
        System.out.println(ENTER_ID);
        try {
            projectService.remove(reader.readLine());
            System.out.println(OBJECT_REMOVED);
        }
        catch(IOException e){
            System.out.println(IO_ERROR);
        }
        catch(IncorrectUUIDException e){
            System.out.println(INCORRECT_ID);
        }
        catch (ObjectNotFoundException e) {
            System.out.println(OBJECT_NOT_FOUND);
        }
    }

    public void projectTasks(){
        System.out.println("[PROJECT ALL TASKS]");
        System.out.println(ENTER_ID);
        try{
            String id = reader.readLine();
            System.out.println(taskService.findAllByProjectId(id));
            System.out.println(OK);
        }
        catch(IOException e){
            System.out.println(IO_ERROR);
        }
        catch(IncorrectUUIDException e){
            System.out.println(INCORRECT_ID);
        }
    }

    public void createTask(BufferedReader reader){
        System.out.println("[TASK CREATE]");
        List list = readData(reader);
        if(list != null && list.size() > 3) {
            try {
                taskService.persist(taskService.createTask(null, list.get(0).toString(), list.get(1).toString(),
                        list.get(2).toString(), list.get(3).toString()));
                System.out.println(OK);
            } catch (IncorrectUUIDException e) {
                System.out.println(INCORRECT_ID);
            }
            catch(IncorrectDataException e){
                System.out.println(INCORRECT_DATE_FORMAT);
            }
            catch (ObjectNotFoundException e) {
                System.out.println(OBJECT_NOT_FOUND);
            } catch (ObjectExistException e) {
                System.out.println("[OBJECT EXIST]\n");
            }
        }
    }

    public void editTask(BufferedReader reader){
        System.out.println("[TASK EDIT]");
        System.out.println(ENTER_ID);
        String id = "";
        try{
            id = reader.readLine();
        }
        catch(IOException e){
            System.out.println(IO_ERROR);
        }
        List list = readData(reader);
        if(list != null && list.size() > 3) {
            try {
                taskService.merge(taskService.createTask(id, list.get(0).toString(), list.get(1).toString(),
                        list.get(2).toString(), list.get(3).toString()));
                System.out.println(OK);
            } catch (IncorrectUUIDException e) {
                System.out.println(INCORRECT_ID);
            }
            catch(IncorrectDataException e){
                System.out.println(INCORRECT_DATE_FORMAT);
            }
            catch (ObjectNotFoundException e) {
                System.out.println(OBJECT_NOT_FOUND);
            }
        }
    }

    public void removeTask(BufferedReader reader){
        System.out.println("[TASK REMOVE]");
        System.out.println(ENTER_ID);
        try {
            taskService.remove(reader.readLine());
            System.out.println(OBJECT_REMOVED);
        }
        catch(IOException e){
            System.out.println(IO_ERROR);
        }
        catch(IncorrectUUIDException e){
            System.out.println(INCORRECT_ID);
        }
        catch (ObjectNotFoundException e) {
            System.out.println(OBJECT_NOT_FOUND);
        }
    }

    public void linkToProject(BufferedReader reader){
        System.out.println("[PROJECT TASKS LIST]\nENTER PROJECT ID:");
        try{
            String projectId = reader.readLine();
            projectService.findOne(projectId);
            System.out.println("ENTER TASK ID:");
            String id = reader.readLine();
            taskService.findOne(id);
            taskService.linkTask(projectId, id);
            System.out.println(OK);
        }
        catch(IOException e){
            System.out.println(IO_ERROR);
        }
        catch(IncorrectUUIDException e){
            System.out.println(INCORRECT_ID);
        }
        catch (ObjectNotFoundException e) {
            System.out.println(OBJECT_NOT_FOUND);
        }
    }

    public List readData(BufferedReader reader) {
        try{
            List list = new LinkedList();
            System.out.println(ENTER_NAME);
            list.add(reader.readLine());
            System.out.println(ENTER_DESCRIPTION);
            list.add(reader.readLine());
            System.out.println(ENTER_DATE_START);
            list.add(reader.readLine());
            System.out.println(ENTER_DATE_END);
            list.add(reader.readLine());
            return list;
        }
        catch(IOException e){
            System.out.println(IO_ERROR);
        }
        return null;
    }

}
