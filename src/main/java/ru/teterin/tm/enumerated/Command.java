package ru.teterin.tm.enumerated;

public enum Command {

    HELP,
    PROJECT_CLEAR,
    PROJECT_CREATE,
    PROJECT_LIST,
    PROJECT_REMOVE,
    PROJECT_EDIT,
    PROJECT_TASKS,
    TASK_CLEAR,
    TASK_CREATE,
    TASK_LIST,
    TASK_REMOVE,
    TASK_EDIT,
    TASK_LINK,
    DEFAULT,
    EXIT

}
