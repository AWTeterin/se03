Task Manager
---
https://gitlab.com/AWTeterin/se03

Software
---
    -JRE
    -Java 1.8
    -Maven 4.0.0

Technology Stack
---
    -Maven
    -Git
    -Java SE

Developer
---
   Teterin Alexei    
   email: [teterin2012@rambler.ru](teterin2012@rambler.ru)

Commands for building the app
---
    mvn clean install
Commands for run the app
---
    java -jar target/se03-1.0.jar